package com.yondry.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(urlPatterns = "/verSession")
public class VerSession extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3617836084203045720L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
		String nombre="";
		nombre = req.getParameter("nombre");
	HttpSession session = req.getSession();
	Object nom =  session.getAttribute(nombre);
	
	PrintWriter out = resp.getWriter();
	out.println("Nombre: "+nom);
	
	}
	
}
