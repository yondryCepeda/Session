package com.yondry.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.eclipse.persistence.sessions.Session;


@WebServlet(urlPatterns = "/crearSession")
public class CrearSession extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6948057622590285298L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String nombre="",valor="";
		
		nombre = req.getParameter("nombre");
		valor = req.getParameter("valor");
		
		HttpSession session = req.getSession();
		
		session.setAttribute(nombre, valor);
		
		PrintWriter out = resp.getWriter();
		
		
		out.println("Se ha creado la session");
	}

	
}
